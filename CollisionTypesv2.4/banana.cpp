#include "banana.h"

//=============================================================================
// default constructor
//=============================================================================
Banana::Banana() : Entity()
{
    spriteData.width = bananaNS::WIDTH;           
    spriteData.height = bananaNS::HEIGHT;
    spriteData.x = bananaNS::X;                   // location on screen
    spriteData.y = bananaNS::Y;
    spriteData.rect.bottom = bananaNS::HEIGHT/2;    // rectangle to select parts of an image
    spriteData.rect.right = bananaNS::WIDTH;
    
	velocity = D3DXVECTOR2(0,0);
    startFrame = 0;              // first frame of ship animation
    endFrame     = 0;              // last frame of ship animation
    currentFrame = startFrame;
    radius = bananaNS::WIDTH/2.0;                 // collision radius
    collision = false;
    collisionType =entityNS::BOX;// entityNS::CIRCLE;
    target = false;
	spriteData.scale = 1;
	active = true;
	speed = 0;
}

bool Banana::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void Banana::setInvisible()
{
	Image::setVisible(false);
	active = false;
}

void Banana::setVisible()
{
	Image::setVisible(true);
	active = true;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Banana::update(float frameTime)
{
	VECTOR2 foo = velocity*frameTime*speed;
	if (getPositionX() + Image::getWidth()*Image::getScale() > GAME_WIDTH)
	{
		setPosition(D3DXVECTOR2(0,getPositionY()));
	}
	if (getPositionX() < 0)
	{
		setPosition(D3DXVECTOR2(GAME_WIDTH-Image::getWidth()*Image::getScale(),getPositionY()));
	}
	if (getPositionY() + Image::getHeight()*Image::getScale() > GAME_HEIGHT)
	{
		setPosition(D3DXVECTOR2(getPositionX(),0));
	}
	if (getPositionY() < 0)
	{
		setPosition(D3DXVECTOR2(getPositionX(),GAME_WIDTH-Image::getHeight()*Image::getScale()));
	}

	velocity = D3DXVECTOR2(0,0);
	incPosition(foo);
	Image::setX(getPositionX());
	Image::setY(getPositionY());
    Entity::update(frameTime);
}

void Banana::evade()
{
	VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	if ((vel.x == 0) || (vel.y == 0)) return;
	
	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	setVelocity(vel);
	
	return;
}

void Banana::vectorTrack()
{
	VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	if ((vel.x == 0) || (vel.y == 0)) return;
	
	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	setVelocity(-vel);
}

void Banana::ai(float time, Entity &t)
{ 
	frameTime = time;
	targetEntity = t;
	if (targetEntity.directionx > 0 && getCenterX() > targetEntity.getCenterX()){
		return;
	}
	else if (targetEntity.directionx < 0 && getCenterX() < targetEntity.getCenterX()) {
		return;
	}
	if (abs(targetEntity.getCenterX() - getCenterX()) < 200) {
		vectorTrack();	
	}
	return;
}