// Programming 2D Games
// Copyright (c) 2011, 2012 by: 
// Charles Kelly
// Collision types demo
// Press '1', '2' or '3' to select collision type for ship.

#include "collisionTypes.h"
#include <string>
using namespace std;
//=============================================================================
// Constructor
//=============================================================================
CollisionTypes::CollisionTypes()
{
	//nothing here, move on
	ghostTime = 0.0f;
	bananaCount = 0;
	lossCheck = false;
	ghostsOn = true;
}

//=============================================================================
// Destructor
//=============================================================================
CollisionTypes::~CollisionTypes()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void CollisionTypes::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError

   if (!monkeyTM.initialize(graphics,MONKEY_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing puck textures"));

   if (!bananaTM.initialize(graphics,BANANA_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "banana initializing puck textures"));

   if (!ghostTM.initialize(graphics,GHOST_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "ghost initializing puck textures"));


   for(int i = 0; i < 50; i++) {
	if (!bananas[i].initialize(this, bananaTM.getWidth(), bananaTM.getHeight(), 0,&bananaTM))
		throw(GameError(gameErrorNS::WARNING, "banana not initialized"));
   }

   for(int i = 0; i < 3; i++) {
	if (!ghosts[i].initialize(this, ghostTM.getWidth(), ghostTM.getHeight(), 0,&ghostTM))
		throw(GameError(gameErrorNS::WARNING, "ghost not initialized"));
   }
	

	if (!monkey.initialize(this, 128, 176, 0,&monkeyTM))
		throw(GameError(gameErrorNS::WARNING, "Monkey not initialized"));
	monkey.setPosition(VECTOR2(100, 100));
	monkey.setCollision(entityNS::BOX);
	monkey.setEdge(COLLISION_BOX_PUCK);
	monkey.setX(monkey.getPositionX());
	monkey.setY(monkey.getPositionY());
	monkey.setScale(.5);

	
	if (!victoryTM.initialize(graphics,WIN_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "ghost initializing puck textures"));
	if (!victory.initialize(graphics,640, 480, 0,&victoryTM))
		throw(GameError(gameErrorNS::WARNING, "Monkey not initialized"));

	if (!defeatTM.initialize(graphics,LOSS_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "ghost initializing puck textures"));
	if (!defeat.initialize(graphics,640, 480, 0,&defeatTM))
		throw(GameError(gameErrorNS::WARNING, "Monkey not initialized"));

	//set banana position
	for(int i = 0; i < 50; i++) {
		bananas[i].setScale(.04);
		bananas[i].setPositionX(rand() % GAME_WIDTH);
		bananas[i].setPositionY(rand() % GAME_HEIGHT);
	}

	//set ghost starting position
	for(int i = 0; i < 3; i++){
		if(i == 0){
			ghosts[i].setPosition(VECTOR2(-100, 240));
			//ghosts[i].setPosition(VECTOR2(100, 240));
			ghosts[i].setVelocity(VECTOR2(5,0));

		}
		else if(i == 1){
			ghosts[i].setPosition(VECTOR2(320, -100));
			//ghosts[i].setPosition(VECTOR2(320, 240));
			ghosts[i].setVelocity(VECTOR2(0,2));
		}
		else if(i == 2){
			ghosts[i].setPosition(VECTOR2(740, 240));
			//ghosts[i].setPosition(VECTOR2(400, 240));
			ghosts[i].setVelocity(VECTOR2(-1, 0));
		}

		ghosts[i].setCollision(entityNS::BOX);
		//ghosts[i].setEdge(COLLISION_BOX_PUCK);
		ghosts[i].setScale(.2);
	}

	states = PLAY;
	//patternsteps
	patternStepIndex = 0;
	/*for (int i = 0; i< maxPatternSteps; i++)
	{
		patternSteps[i].initialize(&bricks);
		patternSteps[i].setActive();
	}*/
	patternSteps[0].setAction(RIGHT);
	patternSteps[0].setTimeForStep(5);
	patternSteps[1].setAction(NONE);
	patternSteps[1].setTimeForStep(.5);
	patternSteps[2].setAction(LEFT);
	patternSteps[2].setTimeForStep(5);
	patternSteps[3].setAction(NONE);
	patternSteps[3].setTimeForStep(.5);
	return;
}

//=============================================================================
// Update all game items
//=============================================================================
void CollisionTypes::update()
{
	switch(states)
	{
	case PLAY:
		
		if(input->isKeyDown(PADDLE_LEFT)) {
				monkey.left();
		}
		if(input->isKeyDown(PADDLE_RIGHT)) {
				monkey.right();
		}
		if(input->isKeyDown(PADDLE_UP)) {
				monkey.up();
		}
		if(input->isKeyDown(PADDLE_DOWN)) {
				monkey.down();
		}
		monkey.update(frameTime);
	
		for (int i = 0; i < 50;i++) {
			bananas[i].update(frameTime);
		}

		for (int i = 0; i < 3;i++) {
			ghosts[i].update(frameTime);
		}

		if (bananaCount == 50){
			states = VICTORY;
			//removes ghosts from screen
			for(int i = 0; i < 3; i++)
				ghosts[i].setInvisible();
			//removes monkey from screen
			monkey.setInvisible();
			//removes bananas from screen
			for(int i = 0; i < 50; i++)
				bananas[i].setInvisible();
		}

		if(lossCheck){
			states = LOSS;
			//removes ghosts from screen
			for(int i = 0; i < 3; i++)
				ghosts[i].setInvisible();
			//removes monkey from screen
			monkey.setInvisible();
			//removes bananas from screen
			for(int i = 0; i < 50; i++)
				bananas[i].setInvisible();
		}
			
		//if (ghostTime <= 15)
			
		//else
			//audio->stopCue(LAUGH);

		ghostTime += frameTime;
		break;
	case VICTORY:
		if (input->isKeyDown(VK_RETURN)){
			for(int i = 0; i < 3; i++){
				ghosts[i].setVisible();
				if(i == 0){
					ghosts[i].setPosition(VECTOR2(-100, 240));
					ghosts[i].setVelocity(VECTOR2(1,0));
				}
				if(i == 1){
					ghosts[i].setPosition(VECTOR2(320, -100));
					ghosts[i].setVelocity(VECTOR2(0,1));
				}
				if(i == 2){
					ghosts[i].setPosition(VECTOR2(740, 240));
					ghosts[i].setVelocity(VECTOR2(-1,0));
				}
			}

			monkey.setVisible();
			monkey.setPosition(VECTOR2(320, 240));

			for(int i = 0; i < 50; i++) {
				bananas[i].setVisible();
				bananas[i].setPositionX(rand() % GAME_WIDTH);
				bananas[i].setPositionY(rand() % GAME_HEIGHT);
			}
			ghostTime = 0;
			bananaCount = 0;
			ghostsOn = true;
			states = PLAY;
		}
		break;
	case LOSS:
		if (input->isKeyDown(VK_RETURN)){
			for(int i = 0; i < 3; i++){
				ghosts[i].setVisible();
				if(i == 0){
					ghosts[i].setPosition(VECTOR2(-100, 240));
					ghosts[i].setVelocity(VECTOR2(1,0));
				}
				if(i == 1){
					ghosts[i].setPosition(VECTOR2(320, -100));
					ghosts[i].setVelocity(VECTOR2(0,1));
				}
				if(i == 2){
					ghosts[i].setPosition(VECTOR2(740, 240));
					ghosts[i].setVelocity(VECTOR2(-1,0));
				}
			}

			monkey.setVisible();
			monkey.setPosition(VECTOR2(320, 240));

			for(int i = 0; i < 50; i++) {
				bananas[i].setVisible();
				bananas[i].setPositionX(rand() % GAME_WIDTH);
				bananas[i].setPositionY(rand() % GAME_HEIGHT);
			}
			ghostTime = 0;
			bananaCount = 0;
			lossCheck = false;
			ghostsOn = true;
			states = PLAY;
		}
		break;

	}
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void CollisionTypes::ai()
{
	/*bricks.ai(frameTime, monkey);
	if (patternSteps[patternStepIndex%4].isFinished()) {
		patternStepIndex++;
	}
	patternSteps[patternStepIndex%4].update(frameTime);
	*/
	if(ghostsOn)
		audio->playCue(LAUGH);

	for (int i = 0; i < 3;i++) {
		ghosts[i].ai(frameTime, monkey, ghostTime, i, ghostsOn);
	}
}

//=============================================================================
// Handle collisions
//=============================================================================
void CollisionTypes::collisions()
{
    collisionVector = D3DXVECTOR2(0,0);
	collision = false;
/*	if (monkey.collidesWith(bricks, collisionVector))
	{
		collision = true;
		puck.changeDirectionY();
		audio->playCue(BEEP1);
	}
*/
	for (int i = 0; i < 50; i++) {
		if (monkey.collidesWith(bananas[i], collisionVector)) {
			collision = true;
			bananas[i].setInvisible();
			bananaCount++;
			OutputDebugStringA(to_string(bananaCount).c_str());
			audio->playCue(BEEP2);
		}
	}

	for(int i = 0; i < 3; i++){
		if(ghosts[i].collidesWith(monkey, collisionVector))
			lossCheck = true;
	}
}

//=============================================================================
// Render game items
//=============================================================================
void CollisionTypes::render()
{
    graphics->spriteBegin();                // begin drawing sprites
	switch(states)
	{
	case PLAY:
		monkey.draw();
		for (int i = 0; i < 50; i++) {
			bananas[i].draw();
		}
		for (int i = 0; i < 3; i++) {
			ghosts[i].draw();
		}
		break;
	case VICTORY:
		victory.draw();
		break;
	case LOSS:
		defeat.draw();
		break;
	}
    graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void CollisionTypes::releaseAll()
{
	monkeyTM.onLostDevice();
	bananaTM.onLostDevice();
	ghostTM.onLostDevice();
	victoryTM.onLostDevice();
	defeatTM.onLostDevice();
    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void CollisionTypes::resetAll()
{
	monkeyTM.onResetDevice();
	bananaTM.onResetDevice();
	ghostTM.onResetDevice();
	defeatTM.onResetDevice();
	victoryTM.onResetDevice();
    Game::resetAll();
    return;
}
