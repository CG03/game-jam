#include "bricks.h"

//=============================================================================
// default constructor
//=============================================================================
Brick::Brick() : Entity()
{
    spriteData.width = brickNS::WIDTH;           
    spriteData.height = brickNS::HEIGHT;
    spriteData.x = brickNS::X;                   // location on screen
    spriteData.y = brickNS::Y;
    spriteData.rect.bottom = brickNS::HEIGHT/2;    // rectangle to select parts of an image
    spriteData.rect.right = brickNS::WIDTH;
    
	velocity = D3DXVECTOR2(0,0);
    startFrame = 0;              // first frame of ship animation
    endFrame     = 0;              // last frame of ship animation
    currentFrame = startFrame;
    radius = brickNS::WIDTH/2.0;                 // collision radius
    collision = false;
    collisionType =entityNS::BOX;// entityNS::CIRCLE;
    target = false;
	spriteData.scale = 1;
	active = true;
	speed = 50;
}

bool Brick::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void Brick::setInvisible()
{
	Image::setVisible(false);
	active = false;
}

void Brick::setVisible()
{
	Image::setVisible(true);
	active = true;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Brick::update(float frameTime)
{
	VECTOR2 foo = velocity*frameTime*speed;
	if (getPositionX() + Image::getWidth()*Image::getScale() > GAME_WIDTH)
	{
		setPosition(D3DXVECTOR2(0,getPositionY()));
	}
	if (getPositionX() < 0)
	{
		setPosition(D3DXVECTOR2(GAME_WIDTH-Image::getWidth()*Image::getScale(),getPositionY()));
	}
	if (getPositionY() + Image::getHeight()*Image::getScale() > GAME_HEIGHT)
	{
		setPosition(D3DXVECTOR2(getPositionX(),0));
	}
	if (getPositionY() < 0)
	{
		setPosition(D3DXVECTOR2(getPositionX(),GAME_WIDTH-Image::getHeight()*Image::getScale()));
	}

	velocity = D3DXVECTOR2(0,0);
	incPosition(foo);
	Image::setX(getPositionX());
	Image::setY(getPositionY());
    Entity::update(frameTime);
}

void Brick::evade()
{
	VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	if ((vel.x == 0) || (vel.y == 0)) return;
	
	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	setVelocity(vel);
	
	return;
}

void Brick::vectorTrack()
{
	VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	if ((vel.x == 0) || (vel.y == 0)) return;
	
	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	setVelocity(-vel);
}

void Brick::ai(float time, Entity &t)
{ 
	frameTime = time;
	targetEntity = t;
	if (targetEntity.directionx > 0 && getCenterX() > targetEntity.getCenterX()){
		return;
	}
	else if (targetEntity.directionx < 0 && getCenterX() < targetEntity.getCenterX()) {
		return;
	}
	if (abs(targetEntity.getCenterX() - getCenterX()) < 200) {
		vectorTrack();	
	}
	return;
}