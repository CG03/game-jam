#include "Ghost.h"

//=============================================================================
// default constructor
//=============================================================================
Ghost::Ghost() : Entity()
{
    spriteData.width = GhostNS::WIDTH;           
    spriteData.height = GhostNS::HEIGHT;
    spriteData.x = GhostNS::X;                   // location on screen
    spriteData.y = GhostNS::Y;
    spriteData.rect.bottom = GhostNS::HEIGHT/2;    // rectangle to select parts of an image
    spriteData.rect.right = GhostNS::WIDTH;
    
	velocity = D3DXVECTOR2(0,0);
    startFrame = 0;              // first frame of ship animation
    endFrame     = 0;              // last frame of ship animation
    currentFrame = startFrame;
    radius = GhostNS::WIDTH/2.0;                 // collision radius
    collision = false;
    collisionType =entityNS::BOX;// entityNS::CIRCLE;
    target = false;
	spriteData.scale = 1;
	active = true;
	speed = 50;
}

bool Ghost::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void Ghost::setInvisible()
{
	Image::setVisible(false);
	active = false;
}

void Ghost::setVisible()
{
	Image::setVisible(true);
	active = true;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Ghost::update(float frameTime)
{
	VECTOR2 foo = velocity*frameTime*speed;

	//velocity = D3DXVECTOR2(0,0);
	incPosition(foo);
	Image::setX(getPositionX());
	Image::setY(getPositionY());
    Entity::update(frameTime);
}

void Ghost::evade()
{
	VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	if ((vel.x == 0) || (vel.y == 0)) return;
	
	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	setVelocity(vel);
	
	return;
}

void Ghost::vectorTrack()
{
	VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	if ((vel.x == 0) || (vel.y == 0)) return;
	
	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	//if (vel.x == 30 || vel.y == 30)
		setVelocity(-vel);
}

void Ghost::shortVectorTrack()
{
	VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	if ((vel.x == 0) || (vel.y == 0)) return;
	
	float length = D3DXVec2Length(&vel);
	if (length < 200)
	{
		D3DXVec2Normalize(&vel, &vel);
		setVelocity(-vel);
	}
}

void Ghost::mediumVectorTrack()
{
	VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	if ((vel.x == 0) || (vel.y == 0)) return;
	
	float length = D3DXVec2Length(&vel);
	if (length < 250)
	{
		D3DXVec2Normalize(&vel, &vel);
		setVelocity(-vel);
	}
}

void Ghost::longVectorTrack()
{
	VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	if ((vel.x == 0) || (vel.y == 0)) return;
	
	float length = D3DXVec2Length(&vel);
	if (length < 1000)
	{
		D3DXVec2Normalize(&vel, &vel);
		setVelocity(-vel);
	}
}

void Ghost::ai(float time, Entity &t, float &timer, int num, bool &on)
{ 
	frameTime = time;
	targetEntity = t;

	if (timer <= 15){
		if(num==0)
			shortVectorTrack();
		else if (num == 1)
			mediumVectorTrack();
		else if (num == 2)
			longVectorTrack();
		on = false;
	}
	else if(timer > 15 && timer <= 25){
		evade();
		on = false;
	}
	else{
		if(num == 0){
			setPositionX(-100);
			setPositionY(240);
			setVelocity(VECTOR2(5,0));
		}
		else if(num == 1){
			setPositionX(320);
			setPositionY(-100);
			setVelocity(VECTOR2(0,2));
		}
		else if(num == 2){
			setPositionX(740);
			setPositionY(240);
			setVelocity(VECTOR2(-1, 0));
			timer = 0;
		}
		on = true;
		
	}

	return;
}